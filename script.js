const tabs = document.querySelectorAll('.tabs-title');
const tabContents = document.querySelectorAll('.tab-content');


tabs.forEach((tab, index) => {
  tab.addEventListener('click', () => {
    
    const selectedTab = tab.getAttribute('data-tab');

    tabs.forEach(tab => {
      tab.classList.remove('active');
  });

  tabs[index].classList.add('active');

    tabContents.forEach(content => {
      content.classList.remove('active');
      if (content.getAttribute('data-content') === selectedTab) {
        content.classList.add('active')};
    });
  });
});